import React, { Component } from 'react';
import CreateLivre from './CreateLivre';
import LivresList from './LivresList';
import axios from "axios";
import _ from 'lodash';
import './App.css';

const livres = [
  {
    id: 1,
    titre: 'Comment coder en React',
    autheur: 'Rafael'
  },
  {
    id: 2,
    titre: 'Comment coder en SpringBoot',
    autheur: 'Mad'
  }
];

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      livres,
      counter: 2
    }
  }  

  render() {
    return (
      <div>
        <h1>Ma bibliotheque</h1>
        <CreateLivre livres={this.state.livres} createLivre={this.createLivre.bind(this)}/>
        <LivresList
            livres={this.state.livres}
            saveLivre={this.saveLivre.bind(this)}
            deleteLivre={this.deleteLivre.bind(this)}
            />
      </div>
    );
  }

  createLivre(titre, autheur) {
    let id = this.nextId();
    this.state.livres.push({
      id,
      titre,
      autheur
    });
    this.setState({ livres: this.state.livres});
    
    // METTRE VOTRE CODE ICI POUR SAUVER UN NOUVEAU LIVRE
  }

  saveLivre(oldTitre, newTitre, newAutheur) {
    const foundLivre=_.find(this.state.livres, livre => livre.titre === oldTitre);
    foundLivre.titre = newTitre;
    foundLivre.autheur = newAutheur;
    this.setState({livres: this.state.livres});
    
    // METTRE VOTRE CODE ICI POUR SAUVER UN LIVRE APRES L'AVOIR EDITÉ
  }

  deleteLivre(titreToDelete) {
    _.remove(this.state.livres, livre => livre.titre === titreToDelete);
    this.setState({livres: this.state.livres});
  }

  nextId() {
    this.setState({ counter: ++this.state.counter})
    return this.state.counter;
  }
}

export default App;
